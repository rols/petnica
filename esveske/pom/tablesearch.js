/* Cette oeuvre est mise à disposition selon le contrat Attribution-ShareAlike 3.0 Unported disponible en ligne http://creativecommons.org/licenses/by-sa/3.0/ ou par courrier postal à Creative Commons, 171 Second Street, Suite 300, San Francisco, California 94105, USA.
 * Auteur : Erwan Gallenne
 * Site web : http://blog.gallenne.fr
 */ 

(function(jQuery) {
	jQuery.fn.tablesearch = function(options) {

		var defaults = {
			search : "./pom/search.png",
			cancel : "./pom/cancel.png",
			casse:false,
			duration:600,
			open:false
		};

		var opts = jQuery.extend(defaults, options);
		var traiterFiltre=function(input,colIndex){
			var table=input.parents('table').first();
			table.find('tr, .table_search_index'+colIndex).each(function(i){
				if(i>0){
					var tr=jQuery(this);
					var td=jQuery(tr.children().get(colIndex));
					if(td.attr('ts')){
						text=td.attr('ts');
					}else{
						text=td.text();
					}
					if(!opts['casse']){
						text=text.toUpperCase();
						input_text=input.val().toUpperCase();
					}else{
						input_text=input.val();
					}
					
					if(tr.attr('ts_nb')==null){
						tr.attr('ts_nb','0');
					}
					if(text.indexOf(input_text)==-1){
						if(!tr.hasClass('table_search_index'+colIndex)){
							tr.fadeOut(opts['duration']);
							tr.addClass('table_search_index'+colIndex);
							tr.attr('ts_nb',parseInt(tr.attr('ts_nb'))+1);
						}
					}else{
						if(tr.hasClass('table_search_index'+colIndex)){
							tr.attr('ts_nb',parseInt(tr.attr('ts_nb'))-1);
							tr.removeClass('table_search_index'+colIndex);
						}
						
						if(tr.attr('ts_nb')=='0'){
							tr.fadeIn(opts['duration']);
						}
					}
				}
			})
		};
		var remove=function(input,colIndex){
			input.parents('table').first().find('.table_search_index'+colIndex).each(function(){
				var tr=jQuery(this);
				tr.attr('ts_nb',parseInt(tr.attr('ts_nb'))-1);
				tr.removeClass('table_search_index'+colIndex);
				if(tr.attr('ts_nb')=='0'){
					tr.show();
				}
			});

			var th=jQuery(input.parents('tr').first().children().get(colIndex));
			th.find('img:visible').remove();
			th.find('img').show();
			input.remove();
		}
		var addInput=function(img,colIndex){
			img.hide();
			var input=jQuery(document.createElement('input')).addClass('tablesearch_input');
			input.keyup(function(key){
				if(key.keyCode == 27){
					remove(input,colIndex)
				}else{
					traiterFiltre(input,colIndex);
				}
			});
			var img_cancel=jQuery(document.createElement('img')).addClass('tablesearch_input').attr('src',opts['cancel']).css('cursor','pointer');
			img_cancel.click(function(){
				remove(input,colIndex);
			});
			img.parents('th').append(input).append(img_cancel);		
			input.focus();	
		};
		this.each(function() {
				th=jQuery(this);
				var img= jQuery(document.createElement('img')).attr('src',opts['search']).css('cursor','pointer');
				var colIndex = th.parent("tr").children().index(th);
				img.click(function(){
					addInput(img,colIndex);
				})
				th.append(img);
				if(opts['open']){
					addInput(img,colIndex);
				};

		});
		return jQuery(this);
	};
})(jQuery);
